/* A simple server in the internet domain using TCP
   The port number is passed as an argument 
   This version runs forever, forking off a separate 
   process for each connection
*/
#include <stdio.h>
#include <stdlib.h>   
#include <string.h>
#include <signal.h> /* signal name macros, and the kill() prototype */
#include <time.h>   
#include <sys/types.h>   // definitions of a number of data types used in socket.h and netinet/in.h
#include <sys/socket.h>  // definitions of structures needed for sockets, e.g. sockaddr
#include <sys/stat.h>
#include <sys/wait.h>	/* for the waitpid() system call */
#include <netinet/in.h>  // constants and structures needed for internet domain addresses, e.g. sockaddr_in


#define BUFF_SIZE 1024
#define TEMP_SIZE 1000
#define TOKEN_LIST_SIZE 255
#define FILE_BUFFER 1024

void sigchld_handler (int s)
{
    while(waitpid(-1, NULL, WNOHANG) > 0);
}

void dostuff (int); 
void parse_getline (int, char *);
char *contentType (char *);
char *headerTime (void);
char *fileCreationTime (char *);

void error (char *msg)
{
    perror(msg);
    exit(1);
}

int main (int argc, char *argv[])
{
     int sockfd, newsockfd, portno, pid;
     socklen_t clilen;
     struct sockaddr_in serv_addr, cli_addr;
     struct sigaction sa;          // for signal SIGCHLD

     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     memset((char *) &serv_addr, 0, sizeof(serv_addr));

     // Port no. may not be 80, 6789, 0-1024
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;

     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");

     listen(sockfd,5);  // 5 = queue length for pending cnxns
     
     clilen = sizeof(cli_addr);
     
     /****** Kill Zombie Processes ******/
     sa.sa_handler = sigchld_handler; // reap all dead processes
     sigemptyset(&sa.sa_mask);
     sa.sa_flags = SA_RESTART;
     if (sigaction(SIGCHLD, &sa, NULL) == -1) {
         perror("sigaction");
         exit(1);
     }
     /*********************************/
     
     while (1) {
         newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
         
         if (newsockfd < 0) 
             error("ERROR on accept");
         
         pid = fork(); //create a new process
         if (pid < 0)
             error("ERROR on fork");
         
         if (pid == 0)  { // fork() returns a value of 0 to the child process
             close(sockfd);
             dostuff(newsockfd);
             exit(0);
         }
         else //returns the process ID of the child process to the parent
             close(newsockfd); // parent doesn't need this 
     } /* end of while */
     return 0; /* we never get here */
}


/******** DOSTUFF() *********************
 There is a separate instance of this function 
 for each connection.  It handles all communication
 once a connnection has been established.
*****************************************/


void dostuff (int sock)
{
  int o, n;
  char *buffer;
  int req_char_count = 0;
  size_t buff_size = BUFF_SIZE;
  char *temp, *token;
  char **token_list;  

  // Allocate two buffers: smaller one to read in request chunks, larger
  // to store chunks as they are read
  temp = calloc (TEMP_SIZE + 1, sizeof(char));
  buffer = calloc (BUFF_SIZE, sizeof(char));

  // Reading and storing the request from client (browser)
  while ( (n = read(sock, temp, TEMP_SIZE)) > 0 ) {
     int buffer_ptr = req_char_count;
     req_char_count += TEMP_SIZE;

     if (req_char_count > buff_size) {
       buff_size *= 2; 
       buffer = realloc ((void *)buffer, buff_size);
     }

     memcpy (&buffer[buffer_ptr], temp, TEMP_SIZE);
    
     if ( !!strstr (buffer, "\r\n\r\n") ) 
       break;

     memset ((void *)temp, 0, TEMP_SIZE + 1);
  }

  if (n < 0) error("Error reading from socket");
  
  // Read the request to the console 
  printf("\n%s\n", buffer);

  // Tokenize the buffer. Each request field will be stored in a char pointer array
  token = strtok ((void *)buffer, "\n");
  int token_idx = 0; 
  token_list = malloc (TOKEN_LIST_SIZE);

  while ( token != NULL ) {

     if ( token_idx > TOKEN_LIST_SIZE) 
       token_list = realloc ((void *)token_list, token_idx * 2);
      
     if ( token_list != NULL ) {
       token_list[token_idx++] = token; 
       token = strtok (NULL, "\n");
     } else {
       error ("Error allocating request field list!");
     }
  }

  // Look for the request field with "GET" in it, and parse the request
  int idx = 0; 

  while ( idx < token_idx ) {
    char *token_line = token_list[idx]; 

    if ( !!strstr (token_line, "GET") ) {  // make sure this is the GET line
      parse_getline(sock, token_line);
      break;
    } 
    
    idx++; 
  }

  free (temp);
  free (buffer);
  free (token);
  free (token_list);
}


void parse_getline (int sock, char *token_line)
{
  char *response, *content_type, *body, *header, *status, *head_date, *modified; 
  int flen, response_len; 
  FILE *fp; 
  char c; 
  int n;

  int fopen_err = 0;  // flag for 404 error

  char *file_delim = "/";
  char *file_name = strstr (token_line, file_delim);

  file_name = strtok (file_name, " "); 

  if ( strlen (file_name) == 1 ) return; 

  // Move forward to ignore slash, discard the rest of the GET line
  file_name += strlen (file_delim); 

  // Get file type from extension
  content_type = contentType (file_name);

  // Open file and read contents
  fp = fopen (file_name, "r"); 

  // Set error flag if fopen fails
  // Otherwise open and read file 
  if ( fp == NULL ) {

    fopen_err = 1; 

  } else {

    if ( fseek( fp, 0, SEEK_END ) != 0) {
      fopen_err = 1; 

    } else {

      flen = ftell (fp);
      rewind (fp);

      // Allocate the file buffer
      body = malloc (flen);
      if ( !body ) error ("Could not allocate file buffer!");
      memset (body, '\0', flen);

      if ( !fread (body, flen, 1, fp) ) {
        fopen_err = 1; 
      }   
    } 

    fclose (fp); 

  }

  // Create header; format depends on whether there was an error or not
  head_date = headerTime (); 

  if ( fopen_err ) {

    body = "404 Not Found";
    flen = strlen (body);

    if ( asprintf (&header, "HTTP/1.1 404 Not Found\r\n"
      "Date: %s\r\n"
      "Content-Type: text/plain\r\n"
      "Content-Length: %d\r\n\r\n", head_date, strlen (body)) < 0 ) 
      error ("Could not format header!");

  } else {

    modified = fileCreationTime (file_name);

    if ( asprintf (&header, "HTTP/1.1 200 Ok\r\n"
      "Date: %s\r\n" 
      "Content-type: %s\r\n"
      "Last-modified: %s\r\n" 
      "Content-Length: %d\r\n\r\n", 
      head_date, 
      content_type, 
      modified, 
      flen) < 0 ) error ("Could not format header!");       

  }
  
  printf ("%s\n", header);

  // Determine length of header + body and alloc space for combined response
  response_len = ( flen + strlen (header) ); 
  response = malloc (response_len + 1);
  if ( !response ) error ("Could not allocate response buffer!");

  memset (response, '\0', (response_len + 1));
  
  // Copy header into response first, then body where header ends
  strcpy (response, header); 
  memcpy ((response + strlen (header)), body, flen); 
  strcpy ((response + response_len), "\0");  // fread doesn't put null byte at end of file

  // Ensure entire file is sent; n will return negative if error occurred, else n bytes sent
  // Otherwise file may be sent in chunks, so need to loop over "send" until it's done
  int send_counter = 0; 
  n = 0;  

  while ( (n >= 0) && (send_counter < response_len) ) {
    n = send (sock, response, response_len, 0); 
    send_counter += n; 
    // printf ("Read %d of %d bytes\n", send_counter, response_len);
  }

  if (n < 0) error ("ERROR writing file contents to socket");

  free (head_date);
  free (header);
  free (response);
  if ( !fopen_err ) free (body);
  
}


char *contentType (char *fname)
{
  char *content_type;
  char *filetype = strrchr (fname, '.'); 

  if ( filetype == NULL ) 
    return "\0";

  filetype++; 

  if ( !!strstr (filetype, "jpg") ) {
    return "image/jpeg";
  } else if ( !!strstr (filetype, "bmp")) {
    return "image/bmp";
  } else if ( !!strstr (filetype, "html") ) {
    return "text/html";
  } else if ( !!strstr (filetype, "txt") || !!strstr (filetype, "text") ) {
    return "text/plain"; 
  } else {
    return "\0";
  }
  
}


char *fileCreationTime (char *fname) 
{
  // System struct that is defined to store info about files
  struct stat attr;
  char *buf; 
  
  if ( stat (fname, &attr) < 0 ) 
    buf = '\0';
  
  if ( (buf = ctime ( &attr.st_mtime )) == NULL )
    printf ("Couldn't get the file time!");

  buf[strlen (buf) -1] = '\0';  // overwrite auto-newline at end of string

  return buf; 
}


char *headerTime () {
  time_t rawtime;
  struct tm *timeinfo; 
  char *buf; 

  if ( time ( &rawtime ) < 0 );
    buf = '\0';

  if ( (timeinfo = gmtime ( &rawtime )) == NULL ) 
    printf ("Couldn't get the current time!");

  int date_len = strlen (asctime (timeinfo));

  if ( (buf = malloc (date_len)) == NULL ) 
    error ("Memory allocation error!");
  strcpy (buf, asctime (timeinfo));

  buf[strlen (buf) - 1] = '\0';
  
  return buf; 
}

